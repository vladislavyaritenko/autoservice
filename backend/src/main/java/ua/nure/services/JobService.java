package ua.nure.services;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import ua.nure.dao.impl.JobImpl;
import ua.nure.entity.Job;

import java.util.List;

@Service
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class JobService {
    public List<Job> getAllJob() {
        return JobImpl.getInstance().getAll();
    }
}
