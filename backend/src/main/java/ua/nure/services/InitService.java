package ua.nure.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import ua.nure.Context;
import ua.nure.constants.ConstantsBackend;
import ua.nure.dao.impl.UserImpl;
import ua.nure.entity.User;
import ua.nure.exceptions.LogInException;
import ua.nure.exceptions.SignUpExecption;
import ua.nure.utils.Password;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

@Service
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class InitService {
    private Context context;

    @Autowired
    public InitService(Context context) {
        this.context = context;
    }

    public void signUp(Map<String, String> parameters) throws SignUpExecption {
        UserImpl userImpl = UserImpl.getInstance();

        String nameUser = parameters.get(ConstantsBackend.NAME_USER);
        String surnameUser = parameters.get(ConstantsBackend.SURNAME_USER);
        String password = parameters.get(ConstantsBackend.PASSWORD);
        String phoneNumber = parameters.get(ConstantsBackend.PHONE_NUMBER);
        String passwordHash = null;
        try {
            passwordHash = Password.hash(password);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        User user = new User(passwordHash, nameUser, surnameUser, phoneNumber);
        if (userImpl.getByPhoneNumber(phoneNumber) != null) {
            throw new SignUpExecption("User with this number already exists");
        }
        if (passwordHash != null) {
            userImpl.create(user);
        }
    }

    public User logIn(Map<String, String> parameters) throws LogInException {
        UserImpl userImpl = UserImpl.getInstance();

        String password = parameters.get(ConstantsBackend.PASSWORD);
        String phoneNumber = parameters.get(ConstantsBackend.PHONE_NUMBER);

        String passwordHash = null;
        try {
            passwordHash = Password.hash(password);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        User user = userImpl.getByPhoneNumber(phoneNumber);

        if ((user != null) && (user.getPassword().equalsIgnoreCase(passwordHash))) {
            user.setPassword(null);
            context.setUser(user);
            return user;
        }

        throw new LogInException("Login data is incorrect!");
    }
}
