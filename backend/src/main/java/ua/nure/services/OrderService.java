package ua.nure.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import ua.nure.Context;
import ua.nure.constants.ConstantsBackend;
import ua.nure.dao.impl.OrderImpl;
import ua.nure.entity.Car;
import ua.nure.entity.Order;
import ua.nure.entity.User;
import ua.nure.entity.bean.OrderBean;
import ua.nure.entity.bean.OrderBeanAdmin;
import ua.nure.exceptions.UserException;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class OrderService {

    private Context context;

    @Autowired
    public OrderService(Context context) {
        this.context = context;
    }

    public int createOrder(Map<String, String> parameters) throws UserException {
        User user = context.getUser();
        if (user == null) {
            throw new UserException("You need to login again!");
        }

        OrderImpl orderImpl = OrderImpl.getInstance();
        String brandAuto = parameters.get(ConstantsBackend.BRAND_AUTO);
        String stateNumber = parameters.get(ConstantsBackend.STATE_NUMBER);
        Timestamp visitDate = null;

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date parsedDate = dateFormat.parse(parameters.get(ConstantsBackend.VISIT_DATE));
            visitDate = new Timestamp(parsedDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        double total = Double.parseDouble(parameters.get(ConstantsBackend.TOTAL));
        String[] servicesId = parameters.get(ConstantsBackend.SERVICES).split(",");

        Order order = new Order(new Timestamp(System.currentTimeMillis()), total, visitDate, user.getId());
        Car car = new Car(brandAuto, stateNumber);
        return orderImpl.create(order, servicesId, car);
    }

    public List<OrderBean> getAllOrders() throws UserException {
        User user = context.getUser();
        if (user == null) {
            throw new UserException("You need to login again!");
        }
        return OrderImpl.getInstance().getAllOrders(user.getId());
    }

    public List<OrderBeanAdmin> getAllOrdersAdmin() throws UserException {
        User user = context.getUser();
        if (user == null) {
            throw new UserException("You need to login again!");
        }
        return OrderImpl.getInstance().getAllOrdersAdmin();
    }

    public void changeStatus(Map<String, String> parameters) throws UserException {
        User user = context.getUser();
        if (user == null) {
            throw new UserException("You need to login again!");
        }

        OrderImpl orderImpl = OrderImpl.getInstance();
        int idOrder = Integer.parseInt(parameters.get(ConstantsBackend.ID));
        String status = parameters.get(ConstantsBackend.STATUS);

        int idStatus = orderImpl.getIdStatusByName(status);

        if (idStatus > 0) {
            orderImpl.updateStatus(idOrder, idStatus);
        }
    }
}
