package ua.nure.constants;

public class ConstantsBackend {
    private ConstantsBackend() {
    }

    public static final String NAME_USER = "name";
    public static final String SURNAME_USER = "surname";
    public static final String PASSWORD = "password";
    public static final String PHONE_NUMBER = "phone";

    public static final String BRAND_AUTO = "brandAuto";
    public static final String STATE_NUMBER = "stateNumber";
    public static final String VISIT_DATE = "visitDate";
    public static final String TOTAL = "total";
    public static final String USER = "user";
    public static final String SERVICES = "services";

    public static final String ID = "id";
    public static final String STATUS = "status";

}
