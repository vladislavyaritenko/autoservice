package ua.nure.controllers;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import ua.nure.dao.impl.UserImpl;

import java.util.List;

@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class UserController {


    @GetMapping(value = "getName")
    @ResponseStatus(HttpStatus.OK)
    public List<String> getName() {
        UserImpl userImpl = UserImpl.getInstance();
        return userImpl.getName(2);

    }
}
