package ua.nure.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import ua.nure.entity.Job;
import ua.nure.services.JobService;

import java.util.List;

@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class JobController {
    private JobService jobService;

    @Autowired
    public JobController(JobService jobService) {
        this.jobService = jobService;
    }

    @GetMapping(value = "getAllJob")
    @ResponseStatus(HttpStatus.OK)
    public List<Job> getAllJob() {
        return jobService.getAllJob();
    }
}
