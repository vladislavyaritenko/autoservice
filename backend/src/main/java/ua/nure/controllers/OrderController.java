package ua.nure.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import ua.nure.entity.bean.OrderBean;
import ua.nure.entity.bean.OrderBeanAdmin;
import ua.nure.exceptions.UserException;
import ua.nure.services.OrderService;

import java.util.List;
import java.util.Map;

@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class OrderController {
    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping(value = "bookService")
    @ResponseStatus(HttpStatus.OK)
    public int createOrder(@RequestBody Map<String, String> parameters) throws UserException {
        return orderService.createOrder(parameters);
    }

    @GetMapping(value = "getAllOrders")
    @ResponseStatus(HttpStatus.OK)
    public List<OrderBean> getAllOrders() throws UserException {
        return orderService.getAllOrders();
    }

    @GetMapping(value = "getAllOrdersAdmin")
    @ResponseStatus(HttpStatus.OK)
    public List<OrderBeanAdmin> getAllOrdersAdmin() throws UserException {
        return orderService.getAllOrdersAdmin();
    }

    @PostMapping(value = "changeStatus")
    @ResponseStatus(HttpStatus.OK)
    public void changeStatus(@RequestBody Map<String, String> parameters) throws UserException {
        orderService.changeStatus(parameters);
    }
}
