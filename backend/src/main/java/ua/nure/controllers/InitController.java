package ua.nure.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import ua.nure.entity.User;
import ua.nure.exceptions.LogInException;
import ua.nure.exceptions.SignUpExecption;
import ua.nure.services.InitService;

import java.util.Map;

@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class InitController {

    private InitService service;

    @Autowired
    public InitController(InitService service) {
        this.service = service;
    }

    @PostMapping(value = "signup")
    @ResponseStatus(HttpStatus.CREATED)
    public void signUp(@RequestBody Map<String, String> parameters) throws SignUpExecption {
        service.signUp(parameters);
    }

    @PostMapping(value = "login")
    @ResponseStatus(HttpStatus.OK)
    public User logIn(@RequestBody Map<String, String> parameters) throws LogInException {
        return service.logIn(parameters);
    }
}
