package ua.nure.exceptions;

public class SignUpExecption extends Exception {

    public SignUpExecption(String message) {
        super(message);
    }

    public SignUpExecption(String message, Throwable cause) {
        super(message, cause);
    }

    public SignUpExecption(Throwable cause) {
        super(cause);
    }
}
