package ua.nure.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "User with this number already exists.")
    @ExceptionHandler(SignUpExecption.class)
    public void handleSignUpException(Exception e) {
        e.printStackTrace();
    }

    @ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "Login data is incorrect!")
    @ExceptionHandler(LogInException.class)
    public void handleLogInException(Exception e) {
        e.printStackTrace();
    }

    @ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "You need to login again!")
    @ExceptionHandler(UserException.class)
    public void handleUserException(Exception e) {
        e.printStackTrace();
    }
}
