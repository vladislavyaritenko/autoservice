package ua.nure.dao;

import ua.nure.entity.Car;
import ua.nure.entity.Order;
import ua.nure.entity.bean.OrderBean;
import ua.nure.entity.bean.OrderBeanAdmin;

import java.util.List;

public interface OrderDao extends GenericDao<Order> {
   int create(Order order, String[] servicesId, Car car);

    List<OrderBean> getAllOrders(int idUser);
    List<OrderBeanAdmin> getAllOrdersAdmin();
    List<String> getNameServiceByOrderId(int idOrder);
    boolean updateStatus(int idOrder, int idStatus);
    int getIdStatusByName(String name);
}
