package ua.nure.dao;

import ua.nure.entity.User;

public interface UserDao extends GenericDao<User> {
    User getByPhoneNumber(String phoneNumber);
}
