package ua.nure.dao;

import ua.nure.entity.Job;

public interface JobDao extends GenericDao<Job> {
}
