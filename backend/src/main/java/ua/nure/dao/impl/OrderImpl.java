package ua.nure.dao.impl;

import ua.nure.dao.OrderDao;
import ua.nure.dao.constants.ConstantsDao;
import ua.nure.dao.utils.DbUtil;
import ua.nure.entity.Car;
import ua.nure.entity.Order;
import ua.nure.entity.bean.OrderBean;
import ua.nure.entity.bean.OrderBeanAdmin;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderImpl extends DbUtil implements OrderDao {
    private static OrderImpl instance;

    private static final String CREATE_NEW_ORDER = "INSERT INTO `order` (idOrder, dateOrder, costOrder, visitDate, Status_idStatus, Car_idCar, User_idUser) VALUES (default, ?,?,?,1,?,?)";
    private static final String CREATE_NEW_CAR = "INSERT INTO `car` (idCar, brand, stateNumber) VALUES (default, ?,?)";
    private static final String INSERT_SERVICE_ORDER = "INSERT INTO `ServiceOrder` (idServiceOrder, Order_idOrder, Service_idService) VALUES (default, ?,?)";
    private static final String GET_ODER_BY_ID_USER = "SELECT idOrder, visitDate, c.stateNumber, s.nameStatus FROM `order` o, status s, car c WHERE o.User_idUser = ? and o.Status_idStatus = s.idStatus and o.Car_idCar = c.idCar";
    private static final String GET_NAME_SERVICE_BY_ORDER_ID = "SELECT service.nameService FROM serviceorder JOIN `order` ON serviceorder.Order_idOrder = `order`.idOrder JOIN service ON serviceorder.Service_idService = service.idService  WHERE `order`.idOrder = ?;";
    private static final String GET_ALL_ORDER_ADMIN = "SELECT idOrder, visitDate, costOrder, u.nameUser, u.surnameUser, u.phoneNumber, c.stateNumber, s.nameStatus FROM `order` o, status s, car c, `user` u WHERE o.User_idUser = u.idUser and o.Status_idStatus = s.idStatus and o.Car_idCar = c.idCar ORDER BY s.idStatus ASC, o.idOrder DESC";
    private static final String GET_ID_STATUS_BY_NAME = "SELECT idStatus FROM status WHERE nameStatus = ?";
    private static final String UPDATE_ORDER_STATUS_ID_BY_ORDER_ID = "UPDATE `order` SET Status_idStatus = ? WHERE idOrder = ?";

    private OrderImpl() {
    }

    public static OrderImpl getInstance() {
        if (instance == null) {
            instance = new OrderImpl();
        }
        return instance;
    }

    @Override
    public boolean updateStatus(int idOrder, int idStatus) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_ORDER_STATUS_ID_BY_ORDER_ID);
            preparedStatement.setInt(1, idStatus);
            preparedStatement.setInt(2, idOrder);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Updating idStatus in order failed, no rows affected.");
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement);
        }
        return false;
    }

    @Override
    public int getIdStatusByName(String name) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(GET_ID_STATUS_BY_NAME);
            preparedStatement.setString(1, name);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                return rs.getInt(ConstantsDao.ID_STATUS);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, rs);
        }
        return 0;
    }

    @Override
    public List<OrderBeanAdmin> getAllOrdersAdmin() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        List<OrderBeanAdmin> orderBeanAdminList = new ArrayList<>();

        try {
            connection = getConnection();
            statement = connection.createStatement();
            rs = statement.executeQuery(GET_ALL_ORDER_ADMIN);
            while (rs.next()) {
                int idOrder = rs.getInt(ConstantsDao.ID_ORDER);
                Timestamp visitDate = rs.getTimestamp(ConstantsDao.VISIT_DATE);
                String nameUser = rs.getString(ConstantsDao.NAME_USER);
                String surnameUser = rs.getString(ConstantsDao.SURNAME_USER);
                String phoneNumberUser = rs.getString(ConstantsDao.PHONE_NUMBER);
                String stateNumber = rs.getString(ConstantsDao.STATE_NUMBER);
                String nameStatus = rs.getString(ConstantsDao.NAME_STATUS);
                double cost = rs.getBigDecimal(ConstantsDao.COST_ORDER).doubleValue();

                List<String> nameServices = getNameServiceByOrderId(idOrder);

                orderBeanAdminList.add(extractOrderBeanAdmin(idOrder, visitDate, cost, stateNumber, nameStatus, nameServices, nameUser, surnameUser, phoneNumberUser));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, statement, rs);
        }

        return orderBeanAdminList;
    }

    @Override
    public int create(Order order, String[] servicesId, Car car) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int idCar;
        int idOrder = 0;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(CREATE_NEW_CAR, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, car.getBrand());
            preparedStatement.setString(2, car.getStateNumber());
            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating car failed, no rows affected.");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    idCar = (int) generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Creating car failed, no ID obtained.");
                }
            }

            preparedStatement = connection.prepareStatement(CREATE_NEW_ORDER, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setTimestamp(1, order.getDateOrder());
            preparedStatement.setBigDecimal(2, BigDecimal.valueOf(order.getCostOrder()));
            preparedStatement.setTimestamp(3, order.getVisitDate());
            preparedStatement.setInt(4, idCar);
            preparedStatement.setInt(5, order.getIdUser());
            affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating order failed, no rows affected.");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    idOrder = (int) generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Creating order failed, no ID obtained.");
                }
            }

            for (String id : servicesId) {
                preparedStatement = connection.prepareStatement(INSERT_SERVICE_ORDER);
                preparedStatement.setInt(1, idOrder);
                preparedStatement.setInt(2, Integer.parseInt(id));
                affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new SQLException("Insert to ServiceOrder table failed, no rows affected.");
                }
            }
            connection.commit();

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement);
        }
        return idOrder;
    }

    @Override
    public List<OrderBean> getAllOrders(int idUser) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<OrderBean> orderBeanList = new ArrayList<>();
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(GET_ODER_BY_ID_USER);
            preparedStatement.setInt(1, idUser);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int idOrder = rs.getInt(ConstantsDao.ID_ORDER);
                Timestamp visitDate = rs.getTimestamp(ConstantsDao.VISIT_DATE);
                String stateNumber = rs.getString(ConstantsDao.STATE_NUMBER);
                String nameStatus = rs.getString(ConstantsDao.NAME_STATUS);

                List<String> nameServices = getNameServiceByOrderId(idOrder);

                orderBeanList.add(extractOrderBean(idOrder, visitDate, stateNumber, nameStatus, nameServices));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, rs);
        }
        return orderBeanList;
    }

    @Override
    public List<String> getNameServiceByOrderId(int idOrder) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<String> nameServices = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(GET_NAME_SERVICE_BY_ORDER_ID);
            preparedStatement.setInt(1, idOrder);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                nameServices.add(rs.getString(ConstantsDao.NAME_SRVICE));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, rs);
        }
        return nameServices;
    }

    private OrderBean extractOrderBean(int idOrder, Timestamp visitDate, String stateNumber, String nameStatus, List<String> nameServices) {
        OrderBean orderBean = new OrderBean();
        orderBean.setId(idOrder);
        orderBean.setVisitDate(visitDate);
        orderBean.setStateNumber(stateNumber);
        orderBean.setStatus(nameStatus);
        orderBean.setListServicesName(nameServices);
        return orderBean;
    }

    private OrderBeanAdmin extractOrderBeanAdmin(int idOrder, Timestamp visitDate, double cost, String stateNumber, String nameStatus, List<String> nameServices, String nameUser, String surnameUser, String phoneNumberUser) {
        OrderBeanAdmin orderBeanAdmin = new OrderBeanAdmin();
        orderBeanAdmin.setId(idOrder);
        orderBeanAdmin.setVisitDate(visitDate);
        orderBeanAdmin.setCost(cost);
        orderBeanAdmin.setStateNumber(stateNumber);
        orderBeanAdmin.setStatus(nameStatus);
        orderBeanAdmin.setListServicesName(nameServices);
        orderBeanAdmin.setNameUser(nameUser);
        orderBeanAdmin.setSurnameUser(surnameUser);
        orderBeanAdmin.setPhoneNumberUser(phoneNumberUser);
        return orderBeanAdmin;
    }
}
