package ua.nure.dao.impl;

import ua.nure.dao.UserDao;
import ua.nure.dao.constants.ConstantsDao;
import ua.nure.dao.utils.DbUtil;
import ua.nure.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserImpl extends DbUtil implements UserDao {
    private static UserImpl instance;

    private UserImpl() {
    }

    private static final String CREATE_USER = "INSERT INTO user(idUser, password, nameUser, surnameUser, phoneNumber, Role_idRole) VALUES(default, ?, ?, ?, ?, 2)";
    private static final String GET_USER_BY_PHONE_NUMBER = "SELECT * FROM user, role WHERE phoneNumber = ? and Role_idRole=idRole";
    private static final String GET_NAME = "select service.nameService from serviceorder JOIN `order` ON serviceorder.Order_idOrder = `order`.idOrder JOIN service ON serviceorder.Service_idService = service.idService  Where `order`.User_idUser = ?";

    public static UserImpl getInstance() {
        if (instance == null) {
            instance = new UserImpl();
        }
        return instance;
    }

    public List<String> getName(int idUser) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<String> names = new ArrayList<>();

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(GET_NAME);
            preparedStatement.setInt(1, idUser);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                names.add(rs.getString(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(connection, preparedStatement, rs);
        }
        return names;
    }

    @Override
    public boolean create(User item) {
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(CREATE_USER);
            pstmt.setString(1, item.getPassword());
            pstmt.setString(2, item.getName());
            pstmt.setString(3, item.getSurname());
            pstmt.setString(4, item.getPhone());
            if (pstmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(con, pstmt);
        }
        return false;
    }

    @Override
    public User getByPhoneNumber(String phoneNumber) {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        User user = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(GET_USER_BY_PHONE_NUMBER);
            pstmt.setString(1, phoneNumber);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                user = extractUser(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(con, pstmt, rs);
        }
        return user;
    }

    private User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt(ConstantsDao.ID_USER));
        user.setName(rs.getString(ConstantsDao.NAME_USER));
        user.setSurname(rs.getString(ConstantsDao.SURNAME_USER));
        user.setPassword(rs.getString(ConstantsDao.PASSWORD));
        user.setPhone(rs.getString(ConstantsDao.PHONE_NUMBER));
        user.setRole(rs.getString(ConstantsDao.NAME_ROLE));
        return user;
    }
}
