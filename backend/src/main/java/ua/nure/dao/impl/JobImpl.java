package ua.nure.dao.impl;

import ua.nure.dao.constants.ConstantsDao;
import ua.nure.dao.utils.DbUtil;
import ua.nure.dao.JobDao;
import ua.nure.entity.Job;
import ua.nure.entity.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JobImpl extends DbUtil implements JobDao  {
    private static JobImpl instance;

    private static final String GET_ALL_JOBS = "SELECT * FROM Service";

    private JobImpl(){}

    public static JobImpl getInstance() {
        if (instance == null) {
            instance = new JobImpl();
        }
        return instance;
    }
    @Override
    public List<Job> getAll() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        List<Job> jobs = new ArrayList<>();
        try {
            connection = getConnection();
            statement = connection.createStatement();
            rs = statement.executeQuery(GET_ALL_JOBS);
            while (rs.next()) {
                jobs.add(extractJob(rs));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(connection, statement, rs);
        }

        return jobs;
    }

    private Job extractJob(ResultSet rs) throws SQLException {
        Job job = new Job();
        job.setId(rs.getInt(ConstantsDao.ID_SERVICE));
        job.setName(rs.getString(ConstantsDao.NAME_SRVICE));
        job.setCost(rs.getBigDecimal(ConstantsDao.COST_SERVICE).doubleValue());
        return job;
    }
}
