package ua.nure.dao.constants;

public class ConstantsDao {
    private ConstantsDao() {
    }

    public static final String ID_USER = "idUser";
    public static final String NAME_USER = "nameUser";
    public static final String SURNAME_USER = "surnameUser";
    public static final String PASSWORD = "password";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String USER_ROLE_ID = "Role_idRole";

    public static final String ID_ROLE = "idRole";
    public static final String NAME_ROLE = "nameRole";

    public static final String ID_SERVICE = "idService";
    public static final String NAME_SRVICE = "nameService";
    public static final String COST_SERVICE = "costService";

    public static final String ID_ORDER = "idOrder";
    public static final String DATE_ORDER = "dateOrder";
    public static final String COST_ORDER = "costOrder";
    public static final String VISIT_DATE = "visitDate";
    public static final String STATUS_ID_STATUS = "Status_idStatus";
    public static final String CAR_ID_CAR = "Car_idCar";
    public static final String USER_ID_USER = "User_idUser";

    public static final String ID_STATUS = "idStatus";
    public static final String NAME_STATUS = "nameStatus";

    public static final String ID_CAR = "idCar";
    public static final String BRAND = "brand";
    public static final String STATE_NUMBER = "stateNumber";
}
