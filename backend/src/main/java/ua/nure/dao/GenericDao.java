package ua.nure.dao;

import java.util.List;

public interface GenericDao<T> {
    default boolean create(T item) {
        return false;
    }

    default T getById(int id) {
        return null;
    }

    default boolean update(T item) {
        return false;
    }

    default boolean delete(int id) {
        return false;
    }

    default List<T> getAll() {
        return null;
    }
}
