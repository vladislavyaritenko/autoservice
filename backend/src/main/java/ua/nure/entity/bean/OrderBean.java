package ua.nure.entity.bean;

import java.sql.Timestamp;
import java.util.List;

public class OrderBean {
    int id;
    List<String> listServicesName;
    Timestamp visitDate;
    String status;
    String stateNumber;

    public OrderBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getListServicesName() {
        return listServicesName;
    }

    public void setListServicesName(List<String> listServicesName) {
        this.listServicesName = listServicesName;
    }

    public Timestamp getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Timestamp visitDate) {
        this.visitDate = visitDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStateNumber() {
        return stateNumber;
    }

    public void setStateNumber(String stateNumber) {
        this.stateNumber = stateNumber;
    }

    @Override
    public String toString() {
        return "OrderBean{" +
                "id=" + id +
                ", listServicesName=" + listServicesName +
                ", visitDate=" + visitDate +
                ", status='" + status + '\'' +
                ", stateNumber='" + stateNumber + '\'' +
                '}';
    }
}
