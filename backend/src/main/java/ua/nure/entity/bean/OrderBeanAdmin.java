package ua.nure.entity.bean;

import java.sql.Timestamp;
import java.util.List;

public class OrderBeanAdmin {
    int id;
    List<String> listServicesName;
    String nameUser;
    String surnameUser;
    String phoneNumberUser;
    Timestamp visitDate;
    String status;
    String stateNumber;
    double cost;

    public OrderBeanAdmin() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getListServicesName() {
        return listServicesName;
    }

    public void setListServicesName(List<String> listServicesName) {
        this.listServicesName = listServicesName;
    }

    public Timestamp getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Timestamp visitDate) {
        this.visitDate = visitDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStateNumber() {
        return stateNumber;
    }

    public void setStateNumber(String stateNumber) {
        this.stateNumber = stateNumber;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getSurnameUser() {
        return surnameUser;
    }

    public void setSurnameUser(String surnameUser) {
        this.surnameUser = surnameUser;
    }

    public String getPhoneNumberUser() {
        return phoneNumberUser;
    }

    public void setPhoneNumberUser(String phoneNumberUser) {
        this.phoneNumberUser = phoneNumberUser;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "OrderBeanAdmin{" +
                "id=" + id +
                ", listServicesName=" + listServicesName +
                ", nameUser='" + nameUser + '\'' +
                ", surnameUser='" + surnameUser + '\'' +
                ", phoneNumberUser='" + phoneNumberUser + '\'' +
                ", visitDate=" + visitDate +
                ", status='" + status + '\'' +
                ", stateNumber='" + stateNumber + '\'' +
                ", cost=" + cost +
                '}';
    }
}
