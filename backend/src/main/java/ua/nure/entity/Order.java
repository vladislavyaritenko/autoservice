package ua.nure.entity;

import java.sql.Timestamp;

public class Order {
    int id;
    Timestamp dateOrder;
    double costOrder;
    Timestamp visitDate;
    int idStatus;
    int idCar;
    int idUser;

    public Order() {
    }

    public Order(Timestamp dateOrder, double costOrder, Timestamp visitDate, int idUser) {
        this.dateOrder = dateOrder;
        this.costOrder = costOrder;
        this.visitDate = visitDate;
        this.idUser = idUser;
    }

    public Order(Timestamp dateOrder, double costOrder, Timestamp visitDate) {
        this.dateOrder = dateOrder;
        this.costOrder = costOrder;
        this.visitDate = visitDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(Timestamp dateOrder) {
        this.dateOrder = dateOrder;
    }

    public double getCostOrder() {
        return costOrder;
    }

    public void setCostOrder(double costOrder) {
        this.costOrder = costOrder;
    }

    public Timestamp getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Timestamp visitDate) {
        this.visitDate = visitDate;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public int getIdCar() {
        return idCar;
    }

    public void setIdCar(int idCar) {
        this.idCar = idCar;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", dateOrder=" + dateOrder +
                ", costOrder=" + costOrder +
                ", visitDate=" + visitDate +
                ", idStatus=" + idStatus +
                ", idCar=" + idCar +
                ", idUser=" + idUser +
                '}';
    }
}
