package ua.nure.entity;

public class Car {
    int id;
    String brand;
    String stateNumber;

    public Car() {
    }

    public Car(String brand, String stateNumber) {
        this.brand = brand;
        this.stateNumber = stateNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getStateNumber() {
        return stateNumber;
    }

    public void setStateNumber(String stateNumber) {
        this.stateNumber = stateNumber;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", stateNumber='" + stateNumber + '\'' +
                '}';
    }
}
