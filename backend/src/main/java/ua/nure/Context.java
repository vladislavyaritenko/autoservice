package ua.nure;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import ua.nure.entity.User;

@Component
@Scope(WebApplicationContext.SCOPE_SESSION)
public class Context {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
