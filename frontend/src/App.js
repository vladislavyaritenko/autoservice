import React, {Component} from 'react';
import LogIn from "./js/components/LogIn";
import UserPage from "./js/components/UserPage";
import AdminPage from "./js/components/AdminPage";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: !!localStorage.getItem('user'),
            user: localStorage.getItem('user') !== null ? JSON.parse(localStorage.getItem('user')).role : '',
        };
    }

    render() {
        let page;
        if (this.state.login) {
            if (this.state.user === 'Admin') {
                page = <AdminPage update={this.logOut}></AdminPage>
            }
            if (this.state.user === 'User') {
                page = <UserPage update={this.logOut}></UserPage>
            }
        } else {
            page = <LogIn updateLogin={this.logIn}/>;
        }
        return (
            <React.Fragment>
                {
                    page
                }
            </React.Fragment>
        );
    }

    logIn = () => {
        let user = JSON.parse(localStorage.getItem('user'));
        this.setState({
            login: !!localStorage.getItem('user'),
            user: user.role,
        });
    };

    logOut = () => {
        localStorage.clear();
        this.setState({
            login: localStorage.getItem('user'),
        });
    };
}

export default App;
