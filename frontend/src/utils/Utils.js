import configuration from "../config/UrlConfiguration";

export function fetchPostBody(mapping, body) {
    return fetch(`http://${configuration.url}:${configuration.port}/${mapping}`, {
        method: "POST",
        credentials: 'include',
        mode: 'cors',
        headers: {'Content-Type': 'application/json'},
        body: body
    });
}

export function fetchPost(mapping) {
    return fetch(`http://${configuration.url}:${configuration.port}/${mapping}`, {
        method: "POST",
        credentials: 'include',
        mode: 'cors',
        headers: {'Content-Type': 'application/json'},
    });
}

export function fetchGet(mapping) {
    return fetch(`http://${configuration.url}:${configuration.port}/${mapping}`, {
        method: "GET",
        credentials: 'include',
        mode: 'cors',
        headers: {'Content-Type': 'application/json',},
    });
}
