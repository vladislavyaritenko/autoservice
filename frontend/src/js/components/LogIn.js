import React from "react";
import InputField from './InputField';
import * as shortid from 'shortid';
import 'react-dropdown/style.css';
import * as Utils from "../utils/Utils";
import '../../css/LogIn.css';
import {Link} from "react-router-dom";

const LogIn = props => {
    onClearDataFromChild();
    return (
        <div className="connect">
            {fields.map(field => <InputField name={field.name} type={field.type} pattern={field.pattern}
                                             placeholder={field.placeholder} maxlength={field.maxlength}
                                             key={shortid.generate()}
                                             ourCallback={onGetDataFromChild}
            />)}
            <div className="connect-footer">
                <div id="LogInButton" className="connect-button button" onClick={() => onClickBtn(props)}> LogIn</div>
            </div>

            <Link to="/signup">
                <div className="field">
                    <div className="button">SignUp</div>
                </div>
            </Link>

        </div>
    );
};

const fields = [
    {name: 'Phone number:', type: 'text', placeholder: '+38', maxlength: "13"},
    {name: 'Password:', type: 'password'},
];

const validate = () => {
    for (const field of fields) {
        if (!field.value) {
            alert(`field ${field.name.replace(':', '')} not set!`);
            return false;
        }

        if (field.value && field.name === 'Phone number:') {
            if (!(/^\+38[0-9]{10}$/.test(field.value))) {
                alert("Phone number is incorrect!");
                return false;
            }
        }
    }
    return true;
};

const onClickBtn = async props => {
    try {
        if (!validate()) return;
        let mapping = "login";
        let body = JSON.stringify({
            password: fields.find(field => field.name === 'Password:').value,
            phone: fields.find(field => field.name === 'Phone number:').value,
        });

        const user = await Utils.fetchPostBody(mapping, body);
        if (user.status === 200) {
            localStorage.setItem('user', JSON.stringify(await user.json()));
            onClearDataFromChild();
            props.updateLogin('login');
        } else if (user.status === 406) {
            alert('Login data is incorrect!');
        } else {
            alert('Another exception');
        }
    } catch (err) {
        alert('Server unavailable!');
    }
};

const onClearDataFromChild = () => {
    fields.forEach(field => {
        field.value = "";
    });
}

const onGetDataFromChild = (value, name) => {
    fields.forEach(field => {
        if (field.name === name) {
            field.value = value;
        }
    });
    return value;
};

export default LogIn;
