import React from "react";

const InputField = props => {
    return (
        <div className="field">
            <span>{props.name}</span>
            <input type={props.type} name={props.name} placeholder={props.placeholder} pattern={props.pattern}  maxLength={props.maxlength} value={props.value}
                   onChange={(e) => {
                       props.ourCallback(e.target.value, props.name);
                   }} min={props.min}/>


        </div>

    );
};

export default InputField;
