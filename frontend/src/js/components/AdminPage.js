import React, {Component} from 'react';
import {Button, Col, Container, Row} from "reactstrap";
import * as Utils from "../utils/Utils";
import * as shortid from "shortid";
import '../../css/AdminPage.css'
import Dropdown from "react-dropdown";

class AdminPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: [],
        }
    }

    async componentDidMount() {
        if (localStorage.getItem("orders")) {
            this.setState({orders: JSON.parse(localStorage.getItem('orders'))});
        } else {
            const orders = await Utils.fetchGet("getAllOrdersAdmin");
            if (orders.status === 200) {
                let ordersJson = await orders.json();
                this.setState({orders: ordersJson});
                localStorage.setItem("orders", JSON.stringify(ordersJson));
            } else if (orders.status === 401) {
                alert('You need logout and after that login again!');
            } else {
                alert("Error loading orders.")
            }
        }
    }

    parseVisitDate = (date) => {
        return new Date(date).toLocaleString();
    }

    createTableOrders(orders) {
        let row = [];
        let column = [];

        column.push(<th key={shortid.generate()}>№ order</th>);
        column.push(<th key={shortid.generate()}>List of service name</th>);
        column.push(<th key={shortid.generate()}>Cost</th>);
        column.push(<th key={shortid.generate()}>Name</th>);
        column.push(<th key={shortid.generate()}>Phone number</th>);
        column.push(<th key={shortid.generate()}>Visit date</th>);
        column.push(<th key={shortid.generate()}>State number</th>);
        column.push(<th key={shortid.generate()}>Order status</th>);
        column.push(<th key={shortid.generate()}></th>);

        row.push(<thead key={shortid.generate()}>
        <tr>{column}</tr>
        </thead>);

        if (orders !== undefined) {
            Object.keys(orders).forEach(order => {
                let children = [];

                children.push(
                    <td key={shortid.generate()}>{orders[order].id}</td>
                );

                children.push(
                    <td key={shortid.generate()}>
                        {orders[order].listServicesName.map(name => <div key={shortid.generate()}><span
                            key={shortid.generate()}>{name}</span> <br/></div>)}
                    </td>
                );

                children.push(
                    <td key={shortid.generate()}>{orders[order].cost}</td>
                );

                children.push(
                    <td key={shortid.generate()}>{orders[order].nameUser} {orders[order].surnameUser}</td>
                );

                children.push(
                    <td key={shortid.generate()}>{orders[order].phoneNumberUser}</td>
                );

                children.push(
                    <td key={shortid.generate()}>{this.parseVisitDate(orders[order].visitDate)}</td>
                );

                children.push(
                    <td key={shortid.generate()}>{orders[order].stateNumber}</td>
                );

                children.push(
                    <td key={shortid.generate()}>
                        <Dropdown key={shortid.generate()} options={this.nameStatus.valueOptions}
                                  value={orders[order].status}
                                  onChange={(e) => this.dropDownValue(e, orders[order].id)}/>
                    </td>
                );

                children.push(
                    <td key={shortid.generate()}>
                        <Button size="sm" color="success" onClick={() => this.changeStatus()}>Изменить</Button>
                    </td>
                );

                row.push(<tbody key={shortid.generate()}>
                <tr>{children}</tr>
                </tbody>);
            });
        }
        return row;
    }

    changeStatus = async () => {
        try {
            let mapping = "changeStatus";
            let body = JSON.stringify({
                id: this.nameStatus.idOrder,
                status: this.nameStatus.value,
            });

            const status = await Utils.fetchPostBody(mapping, body);
            if (status.status === 200) {
                console.log("01")
                this.onClearDataFromDropDown();
                const orders = await Utils.fetchGet("getAllOrdersAdmin");
                if (orders.status === 200) {
                    let ordersJson = await orders.json();
                    this.setState({orders: ordersJson});
                    localStorage.setItem("orders", JSON.stringify(ordersJson));
                } else if (orders.status === 401) {
                    alert('You need logout and after that login again!');
                } else {
                    alert("Error loading orders.")
                }
            } else if (status.status === 401) {
                alert('You need logout and after that login again!');
            } else {
                alert('Another exception');
            }
        } catch (err) {
            alert('Server unavailable!');
        }
    }

    nameStatus = {
        valueOptions: ['new', 'in progress', 'close']
    };

    dropDownValue = (e, idOrder) => {
        this.nameStatus.value = e.value;
        this.nameStatus.idOrder = idOrder;
    };

    onClearDataFromDropDown = () => {
        this.nameStatus.value = "";
        this.nameStatus.idOrder = "";
    }

    render() {
        let user = JSON.parse(localStorage.getItem('user'));
        return (
            <div>
                <Container fluid>
                    <Row>
                        <Col className={"header"}>
                            <b className={"text"}>{user.name} {user.surname}</b>
                            <Button outline color="secondary"
                                    onClick={() => this.props.update()}>Logout</Button>{' '}
                        </Col>
                    </Row>
                    <table>
                        {this.createTableOrders(this.state.orders)}
                    </table>
                </Container>
            </div>
        );
    }
}

export default AdminPage;