import React, {Component} from 'react';
import {Button, Col, Container, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import * as shortid from "shortid";
import InputField from "./InputField";
import * as Utils from "../utils/Utils";

class CartPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            setModal: false,
        }
    }

    toggle = () => {
        this.setState(prevState => ({
            modal: !prevState.modal
        }))
    }


    render() {
        let user = JSON.parse(localStorage.getItem('user'));
        return (
            <Container fluid>
                <Row>
                    <Col className={"header"}>
                        <b className={"text"}>{user.name} {user.surname}</b>
                    </Col>
                </Row>
                {this.getCart()}
                {this.getTotalBlock()}
            </Container>
        );
    }

    validate = () => {
        for (const field of this.fields) {
            if (!field.value) {
                alert(`field ${field.name.replace(':', '')} not set!`);
                return false;
            }

            if (field.value && field.name === 'Visit date:') {
                let date = new Date(field.value);
                let dayDate = date.getDate();
                let monthDate = date.getMonth();
                let yearDate = date.getFullYear();
                let hoursDate = date.getHours();
                let minutesDate = date.getMinutes();

                let today = new Date(this.getCurrentDate());
                let dayToday = today.getDate();
                let monthToday = today.getMonth();
                let yearToday = today.getFullYear();
                let hoursToday = today.getHours();
                let minutesToday = today.getMinutes();

                if (yearDate < yearToday) {
                    alert(`Visit date is incorrect!`);
                    return false;
                }
                if ((yearDate >= yearToday) && (monthDate < monthToday)) {
                    alert(`Visit date is incorrect!`);
                    return false;
                }
                if ((yearDate >= yearToday) && (monthDate === monthToday) && (dayDate < dayToday)) {
                    alert(`Visit date is incorrect!`);
                    return false;
                }
                if ((yearDate >= yearToday) && (monthDate === monthToday) && (dayDate === dayToday)) {
                    if (hoursDate < hoursToday) {
                        alert(`Visit date is incorrect!`);
                        return false;
                    }
                    if ((hoursDate >= hoursToday) && minutesDate < minutesToday) {
                        alert(`Visit date is incorrect!`);
                        return false;
                    }
                }
            }
        }
        return true;
    };

    getIdServices = () => {
        let services = JSON.parse(localStorage.getItem("cartUser"));
        let servicesId = [];
        services.forEach(service => servicesId.push(service.id));
        return servicesId;
    }

    getTimestampVisitDate = (visitDate) => {
        visitDate += ':00';
        let index = visitDate.indexOf('T');
        let firstPart = visitDate.substr(0, index);
        let secondPart = visitDate.substr(index + 1);
        return firstPart + ' ' + secondPart;
    }

    bookService = async () => {
        try {
            if (!this.validate()) return;
            let total = this.getTotal();
            let services = this.getIdServices().join();
            let visitDate = this.getTimestampVisitDate(this.fields.find(field => field.name === 'Visit date:').value)
            let mapping = "bookService";
            let body = JSON.stringify({
                brandAuto: this.fields.find(field => field.name === 'Auto brand:').value,
                stateNumber: this.fields.find(field => field.name === 'State number:').value,
                visitDate: visitDate,
                services: services,
                total: total,
            });

            const order = await Utils.fetchPostBody(mapping, body);
            if (order.status === 200) {
                let idOrder = await order.text();
                this.onClearDataFromChild();
                localStorage.removeItem("cartUser");
                this.toggle();
                alert(`Our order №${idOrder} has been accepted. Our operators will contact you shortly.`);
            } else if (order.status === 401) {
                alert('You need logout and after that login again!');
            } else {
                alert('Another exception');
            }
        } catch (err) {
            alert('Server unavailable!');
        }
    }

    getTotalBlock = () => {
        if (localStorage.getItem("cartUser")) {
            return <Row>
                <Col>
                    <hr/>
                    <b>Total: </b> {this.getTotal()}
                    <br/>
                    <React.Fragment>
                        <Button size="sm" color="success" onClick={this.toggle}>Book</Button>
                        <Modal isOpen={this.state.modal} toggle={this.toggle} className={"modal-xl"}>
                            <ModalHeader toggle={this.toggle}>Booking service</ModalHeader>
                            <ModalBody>
                                {this.fields.map(field => <InputField name={field.name} type={field.type}
                                                                      key={shortid.generate()}
                                                                      ourCallback={this.onGetDataFromChild}
                                />)}
                            </ModalBody>
                            <ModalFooter>
                                <Button color="primary" onClick={() => this.bookService()}>Book</Button>
                                <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                    </React.Fragment>
                </Col>
            </Row>
        }
    }

    getCurrentDate = () => {
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();

        let hh = today.getHours();
        let min = today.getMinutes();

        if (hh < 10) {
            hh = '0' + hh;
        }

        return yyyy + '-' + mm + '-' + dd + 'T' + hh + ':' + min;
    }

    fields = [
        {name: 'Auto brand:', type: 'text'},
        {name: 'State number:', type: 'text'},
        {name: 'Visit date:', type: 'datetime-local'},
    ];

    onClearDataFromChild = () => {
        this.fields.forEach(field => {
            field.value = "";
        });
    }


    onGetDataFromChild = (value, name) => {
        this.fields.forEach(field => {
            if (field.name === name) {
                field.value = value;
            }
        });
        return value;
    };

    getTotal = () => {
        let cart = JSON.parse(localStorage.getItem("cartUser"));
        let total = 0;
        cart.forEach(service => total += service.cost);
        return total;
    }

    getCart = () => {
        let cart = localStorage.getItem("cartUser");
        if (cart) {
            return this.createTableAttributes(JSON.parse(cart))
        } else {
            return <b>Корзина пустая</b>
        }
    }

    createTableAttributes = (carts) => {
        let row = [];
        let column = [];
        let i = 0;
        if (carts.length !== 0) {
            Object.keys(carts).forEach(cart => {
                column.push(
                    <Col className={"serviceText"} key={carts[cart].id} xs={3}>
                        <div>
                            <b>Наименование: </b> {carts[cart].name} <br/>
                            <b>Цена: </b> {carts[cart].cost} <br/>
                        </div>
                    </Col>
                );
                i++;
                if (i === 4) {
                    row.push(<Row className={"view"} key={shortid.generate()}>{column}</Row>);
                    column = [];
                    i = 0;
                }
            });
            row.push(<Row className={"view"} key={shortid.generate()}>{column}</Row>);
        }
        return row;
    }
}

export default CartPage;