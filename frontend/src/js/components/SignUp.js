import React from "react";
import InputField from './InputField';
import * as shortid from 'shortid';
import 'react-dropdown/style.css';
import * as Utils from "../utils/Utils";
import '../../css/LogIn.css';
import {Link} from "react-router-dom";

const SignUp = props => {
    return (
        <div className="connect">
            {fields.map(field => <InputField name={field.name} type={field.type} pattern={field.pattern}
                                             placeholder={field.placeholder} maxlength={field.maxlength}
                                             key={shortid.generate()}
                                             ourCallback={onGetDataFromChildSignUp}
            />)}
            <div className="connect-footer">
                <div id="LogInButton" className="connect-button button" onClick={() => onClickBtn(props)}> SignUp</div>
            </div>

            <div className="field">
                <Link to="/">
                    <div className="button">LogIn</div>
                </Link>
            </div>

        </div>
    );
};

const fields = [
    {name: 'Name:', type: 'text'},
    {name: 'Surname:', type: 'text'},
    {name: 'Phone number:', type: 'text', placeholder: '+38', maxlength: "13"},
    {name: 'Password:', type: 'password'},
];

const validate = () => {
    for (const field of fields) {
        if (!field.value) {
            alert(`field ${field.name.replace(':', '')} not set!`);
            return false;
        }

        if (field.value && field.name === 'Phone number:') {
            if (!(/^\+38[0-9]{10}$/.test(field.value))) {
                alert("Phone number is incorrect!");
                return false;
            }
        }
    }
    return true;
};

const onClickBtn = async props => {
    try {
        if (!validate()) return;
        let mapping = "signup";
        let body = JSON.stringify({
            name: fields.find(field => field.name === 'Name:').value,
            surname: fields.find(field => field.name === 'Surname:').value,
            password: fields.find(field => field.name === 'Password:').value,
            phone: fields.find(field => field.name === 'Phone number:').value,
        });

        const tree = await Utils.fetchPostBody(mapping, body);

        if (tree.status === 201) {
            onClearDataFromChild();
            alert('You are signedIn');
        } else if (tree.status === 406) {
            alert('User with this number already exists');
        } else {
            alert('Login data is incorrect!');
        }
    } catch (err) {
        alert('Server unavailable!');
    }
};

const onClearDataFromChild = () => {
    fields.forEach(field => {
        field.value = "";
    });
}

const onGetDataFromChildSignUp = (value, name) => {
    fields.forEach(field => {
        if (field.name === name) {
            field.value = value;
        }
    });
    return value;
};

export default SignUp;
