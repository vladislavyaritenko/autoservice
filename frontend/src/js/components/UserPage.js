import React, {Component} from "react";
import {Button, Col, Container, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import '../../css/UserPage.css'
import * as Utils from "../utils/Utils";
import * as shortid from 'shortid';
import {Link} from "react-router-dom";

class UserPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            services: [],
            modal: false,
            setModal: false,
            orders: undefined,
        }
    }

    toggle = () => {
        this.getAllOrders().then((ordersState) => this.setState(prevState => ({
                modal: !prevState.modal,
                orders: ordersState
            }))
        );
    }

    async componentDidMount() {
        if (localStorage.getItem("services")) {
            this.setState({services: JSON.parse(localStorage.getItem('services'))});
        } else {
            const services = await Utils.fetchGet("getAllJob");
            if (services.status === 200) {
                let servicesJson = await services.json();
                this.setState({services: servicesJson});
                localStorage.setItem("services", JSON.stringify(servicesJson));
            } else {
                alert("Error loading services.")
            }
        }
    }

    createTableServices() {
        let row = [];
        let column = [];
        let services = this.state.services;
        let i = 0;
        if (services.length !== 0) {
            Object.keys(services).forEach(service => {
                column.push(
                    <Col className={"serviceText"} key={services[service].id} xs={3}>
                        <div>
                            <b>Наименование: </b> {services[service].name} <br/>
                            <b>Цена: </b> {services[service].cost} <br/>
                            <Button size="sm" color="success" onClick={() => this.cart(services[service])}>В
                                корину</Button>
                        </div>
                    </Col>
                );
                i++;
                if (i === 4) {
                    row.push(<Row className={"view"} key={shortid.generate()}>{column}</Row>);
                    column = [];
                    i = 0;
                }
            });
            row.push(<Row className={"view"} key={shortid.generate()}>{column}</Row>);
        }
        return row;
    }

    cart = (key) => {
        let keys = localStorage.getItem("cartUser");
        let contests = [];
        if (keys) {
            contests = JSON.parse(keys);
            if (!this.validate(contests, key)) {
                contests.push(key)
                localStorage.setItem("cartUser", JSON.stringify(contests))
            } else {
                alert("You have already chosen this service.")
            }
        } else {
            contests.push(key)
            localStorage.setItem("cartUser", JSON.stringify(contests))
        }
    };

    validate = (contests, key) => {
        for (let i = 0; i < contests.length; i++) {
            if (contests[i].id === key.id) {
                return true;
            }
        }
        return false;
    }

    getAllOrders = async () => {
        try {
            let mapping = "getAllOrders";
            const orders = await Utils.fetchGet(mapping);
            if (orders.status === 200) {
                return await orders.json();
            } else if (orders.status === 401) {
                alert('You need logout and after that login again!');
            } else {
                alert('Another exception');
            }
        } catch (err) {
            alert('Server unavailable!');
        }
    }

    createTableOrder(orders) {
        let row = [];
        let column = [];

        column.push(<th key={shortid.generate()}>№ order</th>);
        column.push(<th key={shortid.generate()}>List of service name</th>);
        column.push(<th key={shortid.generate()}>Visit date</th>);
        column.push(<th key={shortid.generate()}>State number</th>);
        column.push(<th key={shortid.generate()}>Order status</th>);

        row.push(<thead key={shortid.generate()}>
        <tr>{column}</tr>
        </thead>);

        if (orders !== undefined) {
            Object.keys(orders).forEach(order => {
                let children = [];

                children.push(
                    <td key={shortid.generate()}>{orders[order].id}</td>
                );

                children.push(
                    <td key={shortid.generate()}>
                        {orders[order].listServicesName.map(name => <div key={shortid.generate()}><span
                            key={shortid.generate()}>{name}</span> <br/></div>)}
                    </td>
                );

                children.push(
                    <td key={shortid.generate()}>{this.parseVisitDate(orders[order].visitDate)}</td>
                );

                children.push(
                    <td key={shortid.generate()}>{orders[order].stateNumber}</td>
                );

                children.push(
                    <td key={shortid.generate()}>{orders[order].status}</td>
                );

                row.push(<tbody key={shortid.generate()}>
                <tr>{children}</tr>
                </tbody>);
            });
        }
        return row;
    }

    parseVisitDate = (date) => {
        return new Date(date).toLocaleString();
    }

    render() {
        let user = JSON.parse(localStorage.getItem('user'));
        return (
            <div>
                <Container fluid>

                    <Row>
                        <Col className={"header"}>
                            <b className={"text"}>{user.name} {user.surname}</b>
                            <Link to="/cart">
                                <Button className={"cartButton"} outline color="primary">Cart</Button>
                            </Link>
                            <React.Fragment>
                                <Button className={"cartButton"} outline color="primary"
                                        onClick={this.toggle}>Order statuses</Button>
                                <Modal isOpen={this.state.modal} toggle={this.toggle} className={"modal-xl"}>
                                    <ModalHeader toggle={this.toggle}>Order statuses</ModalHeader>
                                    <ModalBody>
                                        <table>
                                            {this.createTableOrder(this.state.orders)}
                                        </table>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                            </React.Fragment>
                            <Button outline color="secondary"
                                    onClick={() => this.props.update()}>Logout</Button>{' '}
                        </Col>
                    </Row>
                    {this.createTableServices()}
                </Container>

            </div>
        );
    }
}


export default UserPage;