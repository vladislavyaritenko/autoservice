import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css'
import {BrowserRouter as Router, Route} from "react-router-dom";
import CartPage from "./js/components/CartPage";
import UserPage from "./js/components/UserPage";
import SignUp from "./js/components/SignUp";

const routing = (
    <Router>
        <div>
            <Route exact path="/" component={App}/>
            <Route exact path="/cart" component={CartPage}/>
            <Route exact path="/user" component={UserPage}/>
            <Route exact path="/signup" component={SignUp}/>
        </div>
    </Router>
);

ReactDOM.render(routing, document.getElementById('root'));

serviceWorker.unregister();
